/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
export const environment = {
  production: true,

  apiEndpoint: {urlBase: 'http://127.0.0.1', puerto: '8080'},

  auth: { url: 'https://dev-965067.oktapreview.com', issuer: 'default' },
  clientId: '0oad8oso7zYH0gCpQ0h7',
};
