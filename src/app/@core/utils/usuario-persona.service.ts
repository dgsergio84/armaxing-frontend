import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioPersonaService {
    
    constructor(private http: HttpClient) {
    }

    registerUsrPers(user: any): Observable<any> {
        const httpheaders = new HttpHeaders().set('Accept', 'application/json');
        
        return this.http.post(`${environment.apiEndpoint.urlBase}:${environment.apiEndpoint.puerto}/personas`,
                user, {headers: httpheaders, observe: 'response', responseType: 'json'});
    }
  
}
