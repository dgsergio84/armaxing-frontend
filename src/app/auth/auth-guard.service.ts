import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanLoad, Router, ActivatedRouteSnapshot, RouterStateSnapshot, Route } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard  implements CanActivate, CanActivateChild, CanLoad {

  constructor(private authService: AuthService, private router: Router) { }
     
     // CanActivate. Interface that a class can implement to be a guard deciding if a route can be activated.
     canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
         let url: string = state.url;
         // console.log(url);

         return this.authService.checkLogin(url);
     }

     // CanAtivateChild. Interface that a class can implement to be a guard deciding if a child route can be activated.
     canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
         return this.canActivate(route, state);
     }

     // CanLoad. Interface that a class can implement to be a guard deciding if a children can be loaded.
     canLoad(route: Route): boolean {
         let url = `/${route.path}`;

         return this.authService.checkLogin(url);
     }
 
}