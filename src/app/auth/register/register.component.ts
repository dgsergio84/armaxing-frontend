import { Component, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { NbRegisterComponent, NbAuthService, NB_AUTH_OPTIONS, getDeepFromObject } from '@nebular/auth';
import { UsuarioPersonaService } from '../../@core/utils/usuario-persona.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'ngx-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends NbRegisterComponent {

  private validations: any = {
    'curp': {
        'minLength': 8,
        'maxLength': 8,
    },
  };

  constructor(private usrPersonService: UsuarioPersonaService,
        protected service: NbAuthService,
        @Inject( NB_AUTH_OPTIONS ) protected options = {},
        protected cd: ChangeDetectorRef,
        protected router: Router,
        private route: ActivatedRoute ) {

        super( service, options, cd, router );

  }

  register(): void {
    this.submitted = true;

    this.usrPersonService.registerUsrPers( this.user ).subscribe( data => {
        // console.log( data );

        const extras: NavigationExtras = {
                // queryParamsHandling: 'merge',
                preserveFragment: true,
                queryParams: {status: '201', email: this.user.email},
                relativeTo: this.route,
            };
        this.router.navigate( ['../login'], extras);
    }, (err) => {
      this.submitted = false;
      console.error( err );
      switch ( err.status ) {
          case 401: {
              this.errors.push(`La Persona ${this.user.nombre} ${this.user.ape1} no ha sido autorizada para el uso del sistema. Contacte al administrador.`);
              break;
          } default: {
              this.errors.push( `Error interno del servidor. Estamos trabajando para resolver a la brevedad el problema.` );
              break;
          }
      }
    });
}

  getExtraConfigValues( key: string ): any {
    return getDeepFromObject( this.validations, key, null );
  }

}
