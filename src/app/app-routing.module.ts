import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth/auth-guard.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthService } from './auth/auth.service';

const routes: Routes = [
  {
    path: 'pages',
    canActivate: [AuthGuard], // we tell Angular to check the access
    // loadChildren: () => import('app/pages/pages.module')
    //  .then(m => m.PagesModule),
    loadChildren: './pages/pages.module#PagesModule',
  },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
    /* component: NbAuthComponent,
    children: [
      {
        path: '',
        component: NbLoginComponent,
      },
      {
        path: 'login',
        component: NbLoginComponent,
      },
      {
        path: 'register',
        component: NbRegisterComponent,
      },
      {
        path: 'logout',
        component: NbLogoutComponent,
      },
      {
        path: 'request-password',
        component: NbRequestPasswordComponent,
      },
      {
        path: 'reset-password',
        component: NbResetPasswordComponent,
      },
    ], */
  },
  { path: '', redirectTo: 'pages', pathMatch: 'full' },
  { path: '**', redirectTo: 'pages' },
];

const config: ExtraOptions = {
  useHash: false,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config),
    OAuthModule.forRoot(),
  ],
  exports: [RouterModule],
  providers: [
    AuthService,
  ],
})
export class AppRoutingModule {
}
