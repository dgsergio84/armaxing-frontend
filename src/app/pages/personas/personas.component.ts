import { Component, OnInit } from '@angular/core';
import { BaseListComponent } from '../base-list.component';
import { AuthService } from '../../auth/auth.service';
import { HttpClient } from '@angular/common/http';
// import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'ngx-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.scss']
})
export class PersonasComponent extends BaseListComponent implements OnInit {

  constructor( protected authService: AuthService,
    protected http: HttpClient,
    /* private router: Router,
    private route: ActivatedRoute */ ) {

    super( authService, http, `${environment.apiEndpoint.urlBase}:${environment.apiEndpoint.puerto}/personas` );
    
    this.settings.columns = {
      id: {
          title: 'ID',
          width: '5%',
          filter: false,
          sort: false,
          type: 'string',
      },
      nombre: {
          title: 'Nombre',
          width: '25%',
          filter: true,
          type: 'string',
          sort: false,
      },
      ape1: {
          title: 'Apellido 1',
          width: '20%',
          filter: true,
          sort: false,
          type: 'string',
      },
      ape2: {
          title: 'Apellido 2',
          width: '15%',
          filter: true,
          sort: false,
          type: 'string',
      },
      email: {
          title: 'Correo',
          width: '20%',
          filter: true,
          type: 'string',
          sort: false,
      },
      curp: {
        title: 'CURP',
          width: '15%',
          filter: true,
          type: 'string',
          sort: false,
      }
    };
    this.settings.pager = {
          perPage: 5,
          display: true,
    };

  }

  ngOnInit() {
  }

  rowSelect( event: any ) {
    super.rowSelect( event, 'persona' );
  }

  resetFilters() {
        
    this.source.setFilter([], true, false);
        
    const filterElements = document.getElementsByClassName('form-control');
    for (const element of <any>filterElements) {
        // console.log(element.value);
        element.value = '';
    }
    
    this.source.reset(false);
    
  }  

}
