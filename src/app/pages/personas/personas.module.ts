import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasRoutingModule } from './personas-routing.module';
import { PersonasComponent } from './personas.component';
import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NbCardModule, NbAlertModule, NbButtonModule } from '@nebular/theme';

@NgModule({
  declarations: [PersonasComponent],
  imports: [
    ThemeModule,
    CommonModule,
    FormsModule, // Can't bind to 'ngModel' since it isn't a known property of 'input'
    PersonasRoutingModule,
    Ng2SmartTableModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
  ]
})
export class PersonasModule { }
