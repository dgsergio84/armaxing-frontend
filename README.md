# Cloud Computing - Armaxing
## Git
Git es un sistema de control de versiones distribuido gratuito y de código abierto diseñado para manejar todo, desde proyectos pequeños hasta muy grandes, con rapidez y eficiencia.

### Descarga e Instalaci&oacute;n
* [Mac OS](https://git-scm.com/download/mac)
* [Windows](https://git-scm.com/download/win)
* [Linux/Unix](https://git-scm.com/download/linux)

### Git con ssh
Use ssh para evitar autenticación mediante password cuando suba código a su repositorio remoto. Para generar una llave ssh emplee el comando:
```sh
$ ssh-keygen
```
Este comando generará dos archivos que implementan criptografía asímetrica, o de llave pública, para sustituir la autenticación mediante password.
```sh
$ ls ~/.ssh
id_rsa id_rsa.pub
```

### Clonado de un repositorio
Se crea una copia local de trabajo con el comando:
```sh
$ git clone $URL_REPOSITORIO
```
### Crear y/o migrar a un nuevo repositorio
Una vez creado un nuevo repositorio en el sistema `git` de su preferencia, en caso que el directorio ya sea de una copia local de trabajo ejecutar:
```sh
$ cd $REPO_DIR

$ git remote remove origin
```
### Git add & commit
Se proponen cambios (se agragan al área de ensayo o **Index**) mediante
```sh
$ git add <nombre_archivo>

# agregar todos los archivos
$ git add *
```
Es el primer paso en el flujo de trabajo de `git`. Para comprometer estos cambios se emplea
```sh
$ git commit -m "Commit message"
```
Ahora la cabeza de la copia local, o **HEAD**, apunta al último cambio realizado.

### Subiendo cambios
Para conectar un directorio de trabajo local con un repositorio remoto es necesario agregarlo como origen. Una vez que la **HEAD** apunte a los últimos cambios es posible enviarlos al repositorio remoto. Para subir estos cambios al nuevo repositorio se emplean los comandos:
```sh
$ git remote add origin $URL_NUEVO_REPOSITORIO

$ git add .

$ git commit -m "First commit: inicio de proyecto en git"

$ git push origin master
```

### Descargando cambios
Para actualizar la copia local de trabajo a los últimos cambios con que cuente el repositorio remoto git, ejecute
```sh
$ git pull
```

## Node & Node Package Manager (npm)
### Descarga
* [Mac OS / Windows / Linux / Unix](https://nodejs.org/dist/latest-v10.x/)
Una vez descargada e instalada la versión 10.9.0 de Node, será visible a través del siguiente comando:
```sh
$ node -v
v10.9.0

$ npm -v
6.2.0
```

### Comenzar con Node.js
Una vez que se haya instalado Node, podemos construir nuestro primer servidor web. Es necesario crear un archivo con el nombre `app.js`, y pergar el siguiente código:
```ts
const http = require('http');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello World\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
```
Después de haber hecho esto, corra su web server empleando:
```sh
$ node app.js
```
visite [http://localhost:3000](http://localhost:3000), y verá el mensaje **Hello World**

### Caso de uso
Aplicación web desarrollada en `Angular`, un framework para aplicaciones web, de código abierto, mantenido por Google, que se utiliza para crear y mantener aplicaciones web de una sola página

#### Descarga de código
Clonar el repositorio demo
```sh
$ git clone git@gitlab.com:dgsergio84/armaxing-frontend.git
```
Después de la clonación esté completa es necesario instalar las dependencias de esta aplicación a manera de módulos `npm` (npm install)
```sh
$ cd working-copy-dir && npm i
```
#### Correr la copia local de la App
```sh
$ npm start
```
Vaya a [http://localhost:4200](http://localhost:4200)

## Autenticación y Autorización
Puede crear una cuenta, de manera gratuita, para emplear el [Servidor de Identidad, Autenticación y Autorización Okta](https://www.okta.com/). **Okta, Inc.** es una empresa de gestión de acceso e identidad que cotiza en bolsa con sede en San Francisco.

## Java & Maven (mvn)
* [Descargas Java para todos los sistemas operativos](https://www.java.com/es/download/manual.jsp)

### Maven 3.5.4
Maven es una herramienta de software para la gestión y construcción de proyectos Java.

#### Instalaci&oacute;n
* [Windows](https://maven.apache.org/install.html)
* Linux/Unix:

```sh
$ wget https://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz -P /tmp

$ sudo tar xf /tmp/apache-maven-3.5.4-bin.tar.gz -C /opt

$ sudo ln -s /opt/apache-maven-3.5.4 /opt/maven
```

### Configuración (archivo ~/.bash_profile)
Es necesario configurar correctamente las variables de entorno requeridas por Maven y despu&eacute;s cargar dicha configuraci&oacute;n con **source**:
```sh
$ vi ~/.bash_profile
---
# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs
export JAVA_HOME=/usr/java/jdk1.8.0_221-amd64
export M2_HOME=/opt/maven
export MAVEN_HOME=/opt/maven

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH=$M2_HOME/bin:$PATH
---

$ source ~/.bash_profile
```

Una vez descargadas, instaladas y configuradas estas tecnologías se pueden verificar a través de los comandos:
```sh
$ java -version

$ mvn -version
```

### Caso de uso
Interfaz de Programación de Aplicaciones (API) desarrollada en `Java 8` con la tecnología `Spring Boot`. Spring es un framework para el desarrollo de aplicaciones y contenedor de inversión de control: un Contenedor que maneja objetos por ti.

#### Descarga de código
Clonar el repositorio demo
```sh
$ git clone git@gitlab.com:dgsergio84/armaxing-backend.git
```
Después de la clonación esté completa es necesario instalar las dependencias através de `Maven` (mvn install)
```sh
$ cd api-working-copy-dir && mvn install
```
#### Correr la copia local de la API
```sh
$ mvn spring-boot:run
```
Vaya a [http://localhost:8080/personas/select](http://localhost:8080/personas/select)

Después vaya a [http://localhost:8080/personas/person/dgsergio84@gmail.com](http://localhost:8080/personas/person/dgsergio84@gmail.com)

Finalmente visite [http://localhost:8080/personas](http://localhost:8080/personas)

## AWS & Google Cloud 
Amazon Web Services es una colección de servicios de computación en la nube pública que en conjunto forman una plataforma de computación en la nube, ofrecidas a través de Internet por Amazon.com. Es usado en aplicaciones populares como `Dropbox`, `Foursquare`, `HootSuite`.

### Identity and Access Management: `IAM`
Identity and Access Management. Administración de Identidades se denomina a un sistema integrado de políticas y procesos organizacionales que pretende facilitar y controlar el acceso a los sistemas de información y a las instalaciones.​

#### AWS IAM
AWS Identity and Access Management (IAM) le permite la administración de identidades y el acceso a los servicios y recursos de AWS de manera segura.

#### [Cloud IAM](https://cloud.google.com/iam/docs/quickstart?hl=es)
Con Cloud IAM, los administradores autorizan quién puede realizar qué acciones en determinados recursos, lo cual les otorga un control y una visibilidad absolutos para gestionar los recursos en la nube de forma centralizada.

## Regiones y zonas de disponibilidad
### [AWS](https://aws.amazon.com/es/about-aws/global-infrastructure/regions_az/)
Las `regiones` son ubicaciones físicas en todo el mundo donde se agrupan los centros de datos. Llamamos a cada grupo de centros de datos lógicos una zona de disponibilidad (AZ). Una `zona de disponibilidad` (AZ) es uno o más centros de datos discretos con alimentación, redes y conectividad redundantes en una región de AWS.

### [Google Cloud](https://cloud.google.com/docs/geography-and-regions?hl=es-419)
Las `regiones` son áreas geográficas independientes que constan de zonas. Las ubicaciones dentro de las regiones suelen tener latencias de red de ida y vuelta menores a 1 ms en el percentil 95.

Las `zonas` son áreas de implementación dentro de una región para los recursos de Cloud Platform. A las zonas se les debe considerar como dominios con fallas únicos dentro de una región. Si necesitas implementar aplicaciones tolerantes a errores con una alta disponibilidad, debes distribuir tus aplicaciones en varias zonas de una región a fin de protegerte contra fallas inesperadas.

### AWS CLI
La interfaz de línea de comandos (CLI) es una herramienta unificada para administrar los productos de AWS. Solo tendrá que descargar y configurar una única herramienta para poder controlar varios servicios de AWS desde la línea de comando y automatizarlos mediante secuencias de comandos.

#### [Instalación](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv1.html)
Puede verificar que se instaló correctamente mediante:
```sh
$ aws --version
aws-cli/1.11.182 Python/3.6.6 Darwin/15.6.0 botocore/1.7.40
```

#### Configuración
Para poder emplear aws-cli es necesario configurar sus credenciales de usuario, entre otros datos. Esto se puede realizar mediante el comando:
```sh
$ aws configure
```
Esto creará una configuración por defecto (default), sin embargo es posible manejar más de un perfil, en caso de que se cuente con más de una cuenta o usuarios en varias cuentas.
```sh
$ aws configure --profile $PROFILE_NAME
```
Puede revisar los perfiles configurados a través de :
```sh
$ cat ~/.aws/config

$ cat ~/.aws/credentials
```

### SDK de Google Cloud
Es un conjunto de herramientas para Google Cloud Platform (GCP) que incluye las herramientas de línea de comandos.

#### [Instalación](https://cloud.google.com/sdk/install?hl=es-419)
El SDK comienza con una sola configuración llamada default. Puedes establecer propiedades en tu configuración ejecutando el comando:
```sh
$ gcloud init
```
La única configuración `default` es adecuada para la mayoría de los casos prácticos. Sin embargo, también puedes crear configuraciones adicionales y alternar entre ellas según sea necesario con gcloud config configurations activate.

Puedes ver estas propiedades en cualquier otro momento con el comando
```sh
$ gcloud config list
```

Contar con múltiples configuraciones es útil si quieres hacer lo siguiente:
* **Usar varios proyectos**: Puedes crear una configuración distinta para cada proyecto y alternar entre ellas según sea necesario.
* **Usar varias cuentas de autorización**.
* **Realizar tareas independientes en general**: Por ejemplo, puedes usar una configuración para trabajar en una aplicación Google App Engine en un proyecto y administrar instancias de Compute Engine no vinculadas en otro proyecto.

#### Administración de [configuraciones](https://cloud.google.com/sdk/docs/configurations?hl=es-419) y [propiedades](https://cloud.google.com/sdk/docs/properties?hl=es-419).
Para crear una configuración, ejecuta
```sh
$ gcloud config configurations create [NAME]
```
Para hacer una lista de las configuraciones en tu SDK de Cloud, ejecuta
```sh
$ gcloud config configurations list
```
Para activar una configuración de la lista ejecuta
```sh
$ gcloud config configurations activate [NAME]
```
`gcloud config list` siempre mostrará las propiedades de la configuración activa.

Para cambiar la configuración activa para una sola invocación de comando, se puede usar la opción `--configuration` en cualquier comando de gcloud:
```sh
$ gcloud auth list --configuration=[CONFIGURATION_NAME]
```

## Bases de Datos en la Nube
### AWS RDS
```sh
$ aws rds create-db-instance --db-name (string) \
  --db-instance-identifier mydbinstance --allocated-storage 5 \
  --db-instance-class db.m4.large --engine mysql \
  --master-username dbmasteruser --master-user-password (string) \
  --engine-version 5.7 --storage-type standard
```
* Example: The following create-db-instance example launches a new DB instance.
```sh
aws rds create-db-instance \
    --allocated-storage 20 --db-instance-class db.m1.small \
    --db-instance-identifier test-instance \
    --engine mysql \
    --enable-cloudwatch-logs-exports '["audit","error","general","slowquery"]' \
    --master-username master \
    --master-user-password secret99

```
#### Install Maven with Yum on Amazon Linux
```sh
$ sudo wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
```
```sh
sudo sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
```

```sh
sudo yum install -y apache-maven
```

```sh
mvn --version
```


### Cloud SQL
* Para crear una instancia sql de un PostgreSQL server y una Base de Datos en Google Cloud SQL se puede emplear:
```sh
$ gcloud --project=pime-cloud sql instances create postgresql-server \
	--region='us-central1' \
	--database-version=POSTGRES_9_6 \
	--cpu=1 \
	--memory=4GiB \
	--activation-policy=always \
	--availability-type=regional
```

## Virtual Compute Instances

### EC2

```sh
$ aws ec2 create-key-pair --key-name MyKeyPair \
  --output text > MyKeyPair.pem
```

```sh
$ chmod 400 MyKeyPair.pem
```

```sh
$ aws ec2 create-security-group --group-name $MY_SG \
  --description "My security group" --vpc-id $vpc_xxxxxxx
```

```sh
$ aws ec2 run-instances --image-id $ami_xxxxxxxx --count 1 \
  --instance-type t2.micro --key-name $MyKeyPair \
  --security-group-ids $sg_903004f8
```

### Google Compute Engine
Para crear una instancia con `Red Hat Enterprise Linux 8`, ejecute:
```sh
$ gcloud compute instances create example-instance \
      --image-family=rhel-8 --image-project=rhel-cloud \
      --zone=us-central1-a
```

* [**Precios**](https://cloud.google.com/compute/all-pricing?hl=es#custommachinetypepricing)

* [**Open or close server ports**](https://docs.bitnami.com/google/faq/administration/use-firewall/)

## Docker
### [Installing Docker on Amazon Linux 2](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html)

1. Update the installed packages and package cache on your instance.
```sh
$ sudo yum update -y
```
2. Install the most recent Docker Community Edition package.
```sh
$ sudo amazon-linux-extras install docker
```
3. Start the Docker service.
```sh
sudo service docker start
```
4. Add the ec2-user to the docker group so you can execute Docker commands without using sudo.
```sh
$ sudo usermod -a -G docker ec2-user
```
5. Log out and log back in again to pick up the new docker group permissions. You can accomplish this by closing your current SSH terminal window and reconnecting to your instance in a new one. Your new SSH session will have the appropriate docker group permissions. Verify that the ec2-user can run Docker commands without sudo.
```sh 
$ docker info
```

### Instalar Docker en Ubuntu
```sh
$ sudo apt-get remove docker docker-engine docker.io
$ sudo apt-get install     apt-transport-https     ca-certificates     curl     software-properties-common
```
```sh
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```
```sh
$ sudo apt-get update
$ sudo apt-get install docker-ce
```
```sh
$ sudo usermod -a -G docker $USER $ exit
```
```sh
$ docker run hello-world
```

### Iniciar Docker
```sh
$ sudo service docker start
```
```sh
$ sudo docker images
```
```sh
$ sudo docker ps -a
```
```sh
$ docker login
```

### Dockerized DataBase

#### PostgreSQL
```sh
$ docker run --name dockerized-postgres -e \
  POSTGRES_USER=sergio -e POSTGRES_PASSWORD=Sewepro18 \
  -e POSTGRES_DB=sergio -d -p 5432:5432 postgres:9.5
```
```sh
$ docker ps -a
```
```sh
$ psql -h 192.168.2.191 -p 5432 -U sergio
```

#### MySQL
```sh
$ docker run --name dockerized-mysql -e MYSQL_USER=sergio \
  -e MYSQL_PASSWORD=a0213100@H -e MYSQL_DATABASE=wpblogdb \
  -p 3306:3306 -d mysql/mysql-server:5.7
```
```sh
$ mysql -u sergio -p -h 148.216.26.18 wpblogdb
```

# Title
1. sdf
1. sdf
- hello
* hola
```sh
$ echo "hello world"
````
**Negritas** `hola` 